#!/bin/bash

print_this_dir(){
  (
    cd "$(dirname "$0")"
    pwd
  )
}

cmd_up(){
  (
    cd "$(print_this_dir)"
    ruby server.rb -- app=jiki-webrick
  )
}

cmd_down(){
  (
    cd "$(print_this_dir)"
    curl "http://localhost:8092/jiki/shutdown"
  )
}

if [ $# -ge 1 ]; then
  cmd="$1"; shift
else
  cmd="up" # default
fi

case $cmd in
  up )
    cmd_up
  ;; down )
    cmd_down
  ;; * )
    echo "invalid command" >&2
    exit 1
  ;;
esac
