require_relative "helper"

class MalTest < Minitest::Test
  def test_gen_seq_01
    act =
      Mal.eval_v2(
        {},
        <<~MAL
          (gen-seq 3)
        MAL
      )
    assert_equal([0, 1, 2], act)
  end

  def test_gen_seq_tco
    begin
      Mal.eval_v2(
        {},
        <<~MAL
          (gen-seq 10000)
        MAL
      )
      pass
    rescue SystemStackError
      flunk "TCO failed"
    end
  end
end
