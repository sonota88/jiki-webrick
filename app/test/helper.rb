require "minitest/autorun"
require "json"

$LOAD_PATH.unshift(
  File.expand_path("../lib", __dir__)
)

config = JSON.parse(File.read("config.json"))
CONFIG = {
  # "data_root" => File.expand_path(config["data_root"]),
  "mal_dir" => File.expand_path(config["mal_dir"])
}

require "mal"
require "wiki"

def test_tco
  begin
    yield
  rescue SystemStackError => e
    e.backtrace[0...10].each { |trace|
      puts "  |" + trace
    }
    puts "..."
    e.backtrace[-30..-1].each { |trace|
      puts "  |" + trace
    }
    flunk(e)
  end
end
