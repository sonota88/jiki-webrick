/**
 * Wiki processor
 */
(()=>{

  const util = {
    HTML_ESCAPE_MAP: {
      "<": "&lt;"
      ,">": "&gt;"
      ,'"': "&quot;"
      ,"'": "&#39;"
    }

    ,escapeHTML: (src)=>{
      return src
        .replace(/&/g, "&amp;")
        .replace(/[<>"']/g, function(m) {
          return util.HTML_ESCAPE_MAP[m];
        });
    }
  };

  // function puts() { console.log(arguments); }

  function isundef(it) {
    return typeof it === "undefined";
  }

  function _last(xs) {
    return xs[xs.length - 1];
  }

  function xtag(elem, tagName) {
    return elem.getElementsByTagName( tagName );
  }

  function unshift(first, arr) {
    const xs = [ first ];
    for (let a=0,len=arr.length; a<len; a++) {
      xs.push(arr[a]);
    }
    return xs;
  }

  function strip(str) {
    if (!str) {
      return null;
    }
    return str.replace( /^[\s\t\n\r\n]+/, "" ).replace( /[\s\t\r\n]+$/, "" );
  }

  function mkStr(str, n) {
    return new Array(n + 1).join(str);
  }

  function expandTabs(str) {
    return str.replace(
        /\t/g
      , mkStr(" ", 8)
    );
  }

  function formatDate(date) {
    function fmt(n) {
      return "" + (n<10 ? "0" + n : n);
    }

    return date.getFullYear()
      + "-" + fmt(date.getMonth() + 1)
      + "-" + fmt(date.getDate())
      + " " + fmt(date.getHours())
      + ":" + fmt(date.getMinutes());
  }

  // --------------------------------

const WikiProc = {
  idTitleMap: null
  ,getTitleByPageId: function(pageId){
    return this.idTitleMap["" + pageId];
  }

  /**
   * absolue: デフォルト
   * relative: 静的HTMLへの変換用
   */
  ,opts: {
    pagePathType: 'absolute' // or 'relative'
  }
};

function makeHeadingId(htitle) {
  // MediaWiki style
  return encodeURIComponent(htitle.replace(/ /g, "_"))
    .replace(/%/g, ".");
}


class Elem {
constructor(type, content) {
  this.type = type;
  this.list = [];
  this.content = content; // html
}

  toHtml() {
    if (! this.type) {
      return this.content ? lineToHtml(this.content) : "";
    }

    let attr = "";
    if (this.attr) {
      for (let k in this.attr) {
        const v = this.attr[k];
        attr += " " + k + '="' + v + '"';
      }
    }

    let innerHTML;
    if (this.type === "pre") {
      if (this.attr["class"] === "ul") {
        innerHTML = this.content;
      } else {
        // 通常の pre 要素
        innerHTML = util.escapeHTML(this.content);
      }
    } else {
      innerHTML = this.content;
    }

    if (this.type === "hr") {
      return "<" + this.type + " />";
    } else {
      return "<" + this.type + " "
        + attr
        + " >"
        + innerHTML
        + "</" + this.type + ">";
    }
  }
}


function makeEMIndex(formatted) {
  // Index of emphatic text
  let emReference = "";
  const ems = xtag(formatted, "em");
  let emRefElem = null;
  if (ems.length > 0) {
    for (let a=0,len=ems.length; a<len; a++) {
      const id = "emphasis_" + a;
      ems[a].id = id;
      emReference += '<li><a href="#' + id + '">' + ems[a].innerHTML + '</a></li>\n';
    }
    emReference = "<ul>" + emReference + "</ul>";
    emReference = "<h1>Index of emphatic texts</h1>" + emReference;

    emRefElem = createElement(
      null, "div"
      , { id: "emphasis_index" }, {}
      , emReference
    );

    // add to TOC
    createElement(
      document.getElementById("toc").childNodes[0]
      , "li", {}, {}
      , '<a href="#emphasis_index">Index of emphatic texts</a>'
    );
  }

  return emRefElem;
}

// --------------------------------

/**
 * String.prototype.indexOf() のように開始位置を指定して検索する。
 */
function searchFrom(str, re, fromIndex) {
  const idx = str.substring(fromIndex).search(re);
  if (idx < 0) {
    return idx;
  } else {
    return idx + fromIndex;
  }
}

function makePageLink(content) {
  let pageId;
  let title;

  if (content.match( /^(\d+):(.+)$/ )) {
    pageId = parseInt(RegExp.$1, 10);
    title = RegExp.$2;
  } else {
    pageId = parseInt(content, 10);
    title = WikiProc.getTitleByPageId(pageId);
    if (!title) {
      title = "[?]" + content;
    }
  }

  let href;
  switch (WikiProc.opts.pagePathType) {
  case "absolute": 
    href = "/page/" + pageId;
    break;
  case "relative": 
    href = "./" + pageId + ".html";
    break;
  default:
    throw new Error("pagePathType not supported (" + WikiProc.opts.pagePathType + ")");
  }
  return '<a href="' + href + '">'
      + util.escapeHTML(title)
      + '</a>';
}

function _procInline_strong(work) {
  // width start/end
  const ws = 2; // " *"
  const we = 2; // "* "

  const posEnd = searchFrom(work, /\*( |$)/, ws);
  let el;
  let size;

  if (posEnd < 0) {
    el = " *";
    size = ws;
  } else {
    el = '<b>' + work.substring(ws, posEnd) + '</b>';
    size = posEnd + we;
  }

  return [el, size];
}

function _procInline_em(work) {
  const ws = 2; // " _"
  const we = 2; // "_ "

  var posEnd = searchFrom(work, /_( |$)/, ws);
  let el;
  let size;

  if (posEnd < 0) {
    el = " _";
    size = ws;
  } else {
    el = '<em>' + work.substring(ws, posEnd) + '</em>';
    size = posEnd + we;
  }

  return [el, size];
}

function _procInline_code(work) {
  const ws = 2; // " `"
  const we = 2; // "` "

  const posEnd = searchFrom(work, /`( |$)/, ws);
  let el;
  let size;

  if (posEnd < 0) { 
    el = " `";
    size = ws;
  } else {
    el = '<tt>' + work.substring(ws, posEnd) + '</tt>';
    size = posEnd + we;
  }

  return [el, size];
}

function _procInline_link(work) {
  const ws = 3; // " [["
  const we = 3; // "]] "

  var posEnd = searchFrom(work, /\]\]( |$)/, ws);
  let el;
  let size;

  if (posEnd < 0) {
    el = " [[";
    size = ws;
  } else {
    var content = work.substring(ws, posEnd);
    el = makePageLink(content);
    size = posEnd + we;
  }

  return [el, size];
}

function _procInline_url(work) {
  const m = work.match(/^( (https?:\/\/[^ ]+) ?)/);
  const m1 = m[1];
  const url = m[2];

  const el = `<a href="${url}">${url}</a>`;
  size = m1.length;

  return [el, size];
}

function procInline(line) {
  let work = line;
  const els = [];

  // 行頭の処理
  if (     /^\*(.+?)\*( |$)/    .test(work)
        || /^_(.+?)_( |$)/      .test(work)
        || /^`(.+?)`( |$)/      .test(work)
        || /^\[\[(.+?)\]\]( |$)/.test(work)
  ) {
    work = " " + work;
  }

  while (work.length > 0) {
    const pos = work.search( /( \*| _| `| \[\[| https?:\/\/[^ ]+)/ );

    if (pos >= 0) {
      els.push( work.substring(0, pos) ); // left context
      work = work.substring(pos);

      if (/^ \*/.test(work)) {
        const [el, size] = _procInline_strong(work);
        els.push(el);
        work = work.substring(size);

      } else if (/^ _/.test(work)) {
        const [el, size] = _procInline_em(work);
        els.push(el);
        work = work.substring(size);

      } else if (/^ `/.test(work)) {
        const [el, size] = _procInline_code(work);
        els.push(el);
        work = work.substring(size);

      } else if (/^ \[\[/.test(work)) {
        const [el, size] = _procInline_link(work);
        els.push(el);
        work = work.substring(size);

      } else if (/^ (https?:\/\/[^ ]+)/.test(work)) {
        const [el, size] = _procInline_url(work);
        els.push(el);
        work = work.substring(size);

      } else {
        throw new Error("must not happen");
      }
    } else {
      // 行内マークアップがこれ以後存在しない
      els.push(work);
      work = "";
    }
  }

  return els;
}


function lineToHtml(line) {
  let m;

  if (line.match( /^(https?|file):\/\// )) {
    let content = line;
    try {
      content = decodeURIComponent(line);
    } catch (x) {
      ;
    }
    return `<a href="${ line }">${ content }</a>`;
  } else if (line.match( /^link: (.+)/ )) {
    const href = RegExp.$1;
    return `<a href="${ href }">${ href }</a>`;
  } else if (m = line.match( /^\/\/input: (.+)/ )) {
    const val = m[1].replace(/"/g, "&quot;");
    return `<input type="text" value="${ val }" style="width: 90%;" />`;
  } else if (line.match( /^youtube: ((https?|file):\/\/.+$)/ )) {
    const url = RegExp.$1;
    url.match(/v=([^&]+)/);
    const ytid = RegExp.$1;
    return ''
        +'<div class="youtube">'
        +'<iframe width="560" height="315" src="//www.youtube.com/embed/' + ytid + '" frameborder="0" allowfullscreen></iframe>'
        + '<br /><a href="' + url + '">' + url + '</a>'
        + '</div>';
  } else if (line.match( /^img: (.+)$/ )) {
    return '<img src="' + RegExp.$1 + '" />';
  } else {
    return procInline(line).join("");
  }
};


class Parser_v2 {
  static line2elem(line) {
    const elem = new Elem();

    if (line.match( /^---/ )) {
      elem.type = "hr";
      elem.content = "";
    } else {
      elem.content = line;
    }

    return elem;
  }

  static procPRE(slines, sli) {
    // puts("-->> procPRE", slines, sli);

    const slinesPre = [];

    let _class = "indentBlock";
    const sline0 = slines[sli];
    if (sline0.text.match(/^```(.+)/)) {
      _class += " prettyprint lang-" + RegExp.$1;
    }

    while (sli <= slines.length - 1) {
      const sline = slines[sli];
      if (sline.type !== "src") {
        break;
      }
      slinesPre.push(sline);

      sli++;
    }

    // 最初と最後の行を除く
    const slinesInner = slinesPre.slice(1, slinesPre.length - 1);

    const content = slinesInner.map(sl => sl.text).join("\n")

    const elem = new Elem("pre", content);
    elem.attr = {
      "class": _class
    }

    return {
      elem: elem
      , numLines: slinesPre.length
    };
  }

  static procMermaid(lines) {
    lines.shift();

    const _lines = [];

    let line;
    while (lines.length > 0) {
      line = lines.shift();
      if (line.match(/^```/)) {
        break;
      } else {
        _lines.push(line);
      }
    }

    const src = _lines.join("\n");
    const elem = new Elem("div", src);
    elem.attr = { "class": "mermaid" };

    return {
      elem: elem
      , lines: lines
    };
  }

  static parseMain(slines) {
    const node = { list: [] };

    let sli = 0; // slines index

    while (sli <= slines.length - 1) {
      const sline = slines[sli];

      if (sline.type === "src") {
        const ret = Parser_v2.procPRE(slines, sli);
        const elem = ret.elem;
        sli += ret.numLines;
        node.list.push(elem);
      } else if (/^b\{-*$/.test(sline.text)) {
        node.list.push( new Elem(null, '<div class="box">') );
        sli++;
      } else if (/^\}b-*$/.test(sline.text)) {
        node.list.push( new Elem(null, "</div>") );
        sli++;
      } else if (/^q\{-*$/.test(sline.text)) {
        node.list.push( new Elem(null, "<blockquote>") );
        sli++;
      } else if (/^\}q-*$/.test(sline.text)) {
        node.list.push( new Elem(null, "</blockquote>") );
        sli++;
      } else {
        node.list.push( Parser_v2.line2elem(sline.text) );
        sli += 1;
      }
    }

    return { node: node };
  }

  parse(slines) {
    // var lines = slines.map(sline => sline.text);

    const result = Parser_v2.parseMain(slines);
    return result.node;
  };
} // Parser_v2

// function Parser_v1() {

  // 2022-08-09
  // this.getMinHeadSpacesSize = function(lines){
  //   const firstLine = expandTabs(lines[0]);
  //   firstLine.match(/^( +)/);
  //   let minSpaces = RegExp.$1.length;
  // 
  //   for (let a=0,len=lines.length; a<len; a++) {
  //     const line = expandTabs(lines[a]);
  //     if (line.match(/^(\s*)/)) {
  //       const length = RegExp.$1.length;
  //       if (length < minSpaces) {
  //         minSpaces = length;
  //       }
  //     }
  //   }
  //   return minSpaces;
  // };

  // 2022-08-09
  // this.eliminateHeadSpaces = function(lines){
  //   const minSpacesLength = this.getMinHeadSpacesSize(lines);
  //   const headSpaces = mkStr(" ", minSpacesLength);
  //   const headSpacesRE = new RegExp("^" + headSpaces);
  //   return lines
  //     .map(function(line){
  //            return expandTabs(line)
  //              .replace(headSpacesRE, "");
  //          });
  // };

// } // Parser_v1


class Outline {
  constructor(parent, ln0) {
    this.isRoot = (parent == null);
    this.level = this.isRoot ? 0 : parent.level + 1;
    this.kids = [];
    if (! this.isRoot) {
      parent.addKid(this);
    }

    this.title = null;
    this.lineno = 1;
    this.ln0 = ln0;
  }

  addKid(kidOl) {
    this.kids.push(kidOl);
  }

  getLinenoMin() {
    const lns = [];

    lns.push(this.ln0);

    this.kids.forEach((kid)=>{
      if (Array.isArray(kid)) {
        const slines = kid;
        lns.push(slines[0].ln)
      } else {
        lns.push(kid.getLinenoMin());
      }
    });

    return Math.min(...lns);
  }

  getLinenoMax() {
    const lns = [];

    lns.push(this.ln0);

    this.kids.forEach((kid)=>{
      if (Array.isArray(kid)) {
        const slines = kid;
        lns.push(_last(slines).ln)
      } else {
        lns.push(kid.getLinenoMax());
      }
    });

    return Math.max(...lns);
  }

  toPlain() {
    return {
      lv: this.level
      ,lineno: this.lineno
      ,lnFrom2: this.getLinenoMin()
      ,lnTo2: this.getLinenoMax()
      ,title: this.title
      ,range1: `${ this.lineFrom }..${ this.lineTo }`
      ,kids: this.kids.map((kid)=>{
        if (Array.isArray(kid)) {
          const slines = kid;
          const lns = slines.map((sl)=> sl.ln);
          return `range2: ${ lns[0] }..${ _last(lns) }`;
        } else {
          return kid.toPlain();
        }
      })
    };
  }
}


// TODO Use class
function OutlineParser() {

  this.getBlockTitle = function(content){
    const rawTitle = strip(content);
    let title;
    if (rawTitle.match( /^\[notoc\] (.+)/ )) {
      title = strip(RegExp.$1);
    } else {
      title = rawTitle;
    }
    return title;
  };

  this.getBlockShowInToc = function(content){
    return ! /^\[notoc\] (.+)/.test(content);
  };

  this.parse_toSemLines = (src)=>{
    const semLines = [];
    let inSrc = false;

    src.split("\n").forEach((line, i)=>{
      const ln = i + 1;
      let srcLast = false;

      if (line.match(/^```/)) {
        if (inSrc) {
          srcLast = true;
          inSrc = false;
        } else {
          inSrc = true;
        }
      }

      if (inSrc || srcLast) {
        type = "src";
      } else {
        if (line.match(/^=/)) {
          type = "heading";
        } else {
          type = "plain";
        }
      }

      semLines.push({
        ln
        ,type
        ,text: line
      });
    });

    return semLines;
  };

  this.__find = (idx, linenos)=>{
      for (let i=0,len=linenos.length; i<len; i++) {
        const ln = linenos[i];
        if (ln.idx === idx) {
          return ln;
        }
      }
      return null;
    };

  this.__addLineRange = (sec, linenos)=>{
      const ln = this.__find(sec.index, linenos);

      if (ln) {
        sec.lineFrom = ln.from;
        sec.lineTo = ln.to;
      }

      sec.kids.forEach((kid)=>{
        // if(typeof kid !== "string"){
        if (! Array.isArray(kid)) {
          this.__addLineRange(kid, linenos);
        }
      });
    };

  this.parse = function(src){
    const slines = this.parse_toSemLines(src);

    // var level = 0;
    let index = 1;

    const root = new Outline(null, 1);
    let current = root;

    const stack = [];
    var buf = [];
    var linenos = [];
    let lineno;

    slines.forEach((sline)=>{
      lineno = sline.ln;

      if (sline.type === "heading") {
        if (buf.length > 0) {
          current.kids.push(buf);
          buf = [];
        }

        const head = this.procHn(sline.text);
        head.lineno = lineno;
        const delta = head.level - current.level;

        if (0 < delta) {
          for (let a=0; a<delta; a++) {
            stack.push(current);
            current = new Outline(current, lineno);
          }
        } else if (delta < 0) {
          for (let a=0; a<-delta; a++) {
            stack.pop();
          }
          current = new Outline(_last(stack), lineno);
        } else {
          current = new Outline(_last(stack), lineno);
        }

        current.index = index; index++;
        current.lineno = head.lineno;
        current.showInToc = this.getBlockShowInToc(head.content);
        current.title = this.getBlockTitle(head.content);

        linenos.push({ lv: head.level, from: lineno, idx: current.index});

      } else {
        buf.push(sline);
      }
    });

    const linenoMax = lineno;

    if (buf.length > 0) {
      current.kids.push(buf);
    }

    linenos.forEach(function(ln1, i){
      ln1.to = null;

      const nextHead = linenos.filter((ln2, i2)=>{
        return i2 > i && ln2.lv <= ln1.lv;
      })[0];

      if (nextHead) {
        ln1.to = nextHead.from - 1;
      } else {
        ln1.to = linenoMax;
      }
    });

    this.__addLineRange(root, linenos);

    // puts( JSON.stringify(root.toPlain(), null, "  ") );

    return root;
  }

  this.procHn = function(line){
    if (line.match(/^(=+)([^=].*?)=*$/)) {
      return {
        level: RegExp.$1.length
        , content: RegExp.$2
      };
    } else {
      return null;
    }
  };

  this.markup = function(doc){
    let result = "";
    const list = doc.list;

    let elem;
    let nextElem;
    let temp;
    for (let a=0,len=list.length; a<len; a++) {
      elem = list[a];
      nextElem = list[a+1];
      temp = elem.toHtml();
      result += temp;

      if (temp === '<div class="box">'
          || temp.match(/^<\/?blockquote/)
          || temp.match(/^<\/?pre/)
          || (elem.type === "hr" || (nextElem && nextElem.type === "hr"))
       ) {
          //
        } else {
          //result += "<br />";
          result += "\n";
        }
    }

    return result;
  };

  this.toHTMLElementLeaf = function(block){
    const src = block;
    const parser = new Parser_v2();
    const parsed = parser.parse(src);
    return '<div class="outline">' + this.markup(parsed) + '</div>';
  };

  this.isNode = function(obj){
    return (obj.kids != null);
  };

  this.toHTMLElement = function(block, pageId){
    let html = '<div class="outline">';

    if (block.title) {
      let klass = "";
      if (! block.showInToc) {
        klass = 'class="notoc"';
      }
      const hid = makeHeadingId(block.title);
      html += '<h' + block.level + ' id="' + hid + '\" ' + klass + ">";

      html += block.title;
      html += ` (ln0=${block.ln0}) `;
      html += ' <a href="#' + hid + '">&para;</a>';

      const lineRange2 = block.getLinenoMin() + "," + block.getLinenoMax();
      const href2 = `/page/${pageId}/edit?range=${lineRange2}`;
      html += ` <a class="edit_section" href="${href2}">edit(${lineRange2})</a>`;

      html += "</h" + block.level + ">";
    }

    for (let a=0,len=block.kids.length; a<len; a++) {
      const kid = block.kids[a];
      if (this.isNode(kid)) {
        html += this.toHTMLElement(kid, pageId);
      } else {
        html += this.toHTMLElementLeaf(kid, pageId);
      }
    }

    return html + "</div>";
  };
}


function splitPreamble(src) {
  const lines = src.split("\n");
  const info = {};
  const preamble_range = 20;

  for (let a=0; a<preamble_range; a++) {
    if (!lines[a]) { continue; }
    if (lines[a].match(/^title:(.+)/)) {
      info.title = RegExp.$1;
      delete lines[a];
    } else if (lines[a].match(/^by:(.+)/)) {
      info.by = RegExp.$1;
      delete lines[a];
    } else if (lines[a].match(/^date:(.+)/)) {
      info.date = RegExp.$1;
      delete lines[a];
    }
  }

  const _lines = [];
  for (let a=0,len=lines.length; a<len; a++) {
    const line = lines[a];
    typeof line !== "undefined" && _lines.push(line);
  }

  return {
    info: info
    , body: _lines.join("\n")
  };
}


function makePreamble(info) {
  const lines = [];
  info.by && lines.push( "by: " + info.by);
  info.date && lines.push( "date: " + info.date );
  lines.push( "last modified: " + formatDate(new Date(document.lastModified)) );

  const preamble = createElement(
    null, "pre"
    , { "class": "preamble" }, {}
    , lines.join("\n")
  );

  return preamble;
}


function getTitle(info) {
  let title = "untitled";
  if (info.title) {
    title = info.title;
    info.by && (title += " by " + info.by);
    info.date && (title += " (" + info.date + ")");
  }

  return title;
}

// --------------------------------

function printOutline(ol) {
  let ind0 = "";
  let ind1 = "";
  for (let i=0; i<ol.level * 3; i++) {
    ind0 += " ";
  }
  for (let i=0; i<(ol.level+1) * 3; i++) {
    ind1 += " ";
  }
  function p0(x) {
    puts(ind0 + x);
  }
  function p(x) {
    puts(ind1 + x);
  }
  p0("{");
  p(ol.level + ":" + ol.title);
  p("index=" + ol.index);
  p("## lineno=" + ol.lineno);
  p("## " + ol.lineFrom + "--" + ol.lineTo);
  // p(ol.children.length);
  ol.children.forEach(function(kid, i){
    if (typeof kid === "string") {
      p("(text)");
    } else {
      printOutline(kid);
    }
  });
  p0("}");
}

function toHTML(pageId, src, idTitleMap, opts) {
  WikiProc.idTitleMap = idTitleMap;

  opts = opts || {};
  if (opts.pagePathType) {
    WikiProc.opts.pagePathType = opts.pagePathType;
  }

  const olParser = new OutlineParser();
  const outline = olParser.parse(src);
  // puts(">>================================");
  // printOutline(outline);
  // puts("<<================================");
  const wikiPage = {
    mainContent: olParser.toHTMLElement(outline, pageId)
  };

  return wikiPage;
}

  window.wikiproc = {
    toHTML: toHTML
    ,_OutlineParser: OutlineParser
  };
})();

// exports.wikiproc = {
//   toHTML: toHTML
//   ,_OutlineParser: OutlineParser
// };
