const h = TreeBuilder.h;

class View {
  static render(state){
    return h("div", {}
      , h("h1", {}, state.title)
      , h("a", { href: `/page/${__p.getPageId()}/edit` }, "編集")
      , " / "
      , h("input", { value: `[[${__p.getPageId()}:${state.title}]]` })
      , h("hr")

      , h("div", { id: "toc" }
        , h("div", { id: "toc_heading" }, "目次")
        )

      // , h("pre", {}, __p.toHtml())
      , h("pre"
        , { id: "article_main" }
        , TreeBuilder.buildRawHtml(__p.toHtml())
        )
      , h("h1", {}, "このページを参照しているページ")
      , h("ul", {}
        , state.invertedLinks.map(pid =>
            h("li", {}
            , h("a", { href: `/page/${pid}` }, state.pageIdTitleMap[pid])
            )
          )
        )

      , h("h1", {}, "末尾に追記")
      , h("textarea", { id: "last_memo"
          , style: { width: "90%", height: "12rem" }
        })
      , h("br")
      , h("button", {
            onclick: (ev)=> { __p.onclick_saveLastMemo(); }
          }
        , "保存"
        )
    );
  }
}

class Page {
  constructor() {
    this.state = {
      title: '{title}'
      ,src: '{html}'
      ,pageIdTitleMap: {} // TODO
    };
  }

  getTitle() {
    return this.state.title;
  }

  getPageId() {
    location.href.match(/page\/(\d+)/);
    return _parseInt(RegExp.$1);
  }

  init() {
    puts("init");
    __g.api_v2("get", "/api/page/" + this.getPageId(), {
      }, (result)=>{
      __g.unguard();
      puts(result);
      Object.assign(this.state, result);

      this.render();
      __g.updatePageTitle(this.getTitle());

      this.updateLinks();
    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
      alert("Check console.");
    });
  }

  render() {
    $("#tree_builder_container")
      .empty()
      .append(View.render(this.state));

    prettyPrint();

    $("#toc")
      .empty()
      .append(__g.mkToc($("#article_main").get(0)));
  }

  toHtml() {
    try{
      const ret = wikiproc.toHTML(
        this.getPageId()
        ,this.state.src
        ,this.state.pageIdTitleMap
        ,{}
      );
      puts(64, ret);
      // return "{html}";
      return ret.mainContent;
    }catch(e){
      console.error(e);
      return "error";
    }
  }

  updateLinks() {
    const uniq = (xs) => {
      const obj = {};
      xs.forEach(x => { obj[x] = null; });
      return Object.keys(obj);
    };

    const pageId = this.getPageId();

    if (sessionStorage.getItem("changedPageId") == null) {
      return;
    }

    const changedPageId =
          _parseInt(
            sessionStorage.getItem("changedPageId")
          );
    puts("changedPageId", changedPageId);

    // assert
    if (changedPageId !== pageId) {
      return;
    }

    if (changedPageId != null) {
      const ids =
        Array.from($("#article_main").find("a"))
        .map(link => link.href)
        .filter(href => /\/page\//.test(href))
        .filter(href => ! /\/page\/(.+)\/edit/.test(href))
        .map(href => {
          const m = href.match(/\/page\/(\d+)/);
          if (m) {
            return m[1];
          } else {
            return null;
          }
        })
        .filter(id => id != null)
        .filter(id => _parseInt(id) != changedPageId)
        ;

      __g.api_v2(
        "patch", `/api/page/${pageId}/links`,
        {
          destIds: uniq(ids).map(_parseInt)
        },
        (result)=>{
          __g.unguard();
          puts(137, result);
          sessionStorage.removeItem("changedPageId");
        },
        (errors)=>{
          __g.unguard();
          __g.printApiErrors(errors);
          alert("Check console.");
        }
      );
    }
  }

  validSrc_p(src) {
    return __g.validSrc_p(this.getPageId(), src);
  }

  // events

  onclick_saveLastMemo() {
    const src = __p.state.src;
    const text = __g.getById("last_memo").value;

    const blocks = [src];
    const timestamp = new Date().toLocaleString(); // TODO
    blocks.push("\n\n");
    blocks.push("----");
    blocks.push(timestamp);
    blocks.push("");
    blocks.push(text);
    blocks.push("");
    blocks.push(timestamp);
    blocks.push("----");

    const newSrc = blocks.join("\n");

    puts("submit");
    __g.guard();

    if (! this.validSrc_p(newSrc)) {
      alert("invalid src");
      __g.unguard();
      return;
    }

    const params = {
      title: this.state.title
      ,src: newSrc
      // ,range: this.state.range // all
    };

    __g.api_v2(
      "patch"
    , `/api/page/${ __p.getPageId() }`
    , params
    , (result)=>{
        puts("OK");
        location.reload();
      }
    , (errors)=>{
        __g.unguard();
        __g.printApiErrors(errors);
        alert("Check console.");
      }
    );
  }
}

__g.ready(new Page());
